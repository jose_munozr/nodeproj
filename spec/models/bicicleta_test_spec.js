var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");

describe('', () => {
    beforeEach((done) => {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Connection error: '));
        db.once('open', () => {
            console.log('We are connected to test database: ');
            done();
        });
    });

    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [3.4, -76.5]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(3.4);
            expect(bici.ubicacion[1]).toEqual(-76.5);


        });

        describe('Bicicleta.allBicis', () => {
            it('comienza vacia', (done) => {
                Bicicleta.allBicis((err, bicis) => {
                    expect(bicis.length).toBe(0);
                    done();
                });
            });
        });

        describe('Bicicleta.add', () => {
            it('Agregar solo una bici', (done) => {
                var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
                Bicicleta.add(aBici, (err, newBici) => {
                    if (err) console.log(err);
                    Bicicleta.allBicis((err, bicis) => {
                        expect(bicis.length).toEqual(1);
                        expect(bicis[0].code).toEqual(aBici.code);

                        done();
                    })
                })
            });

            describe('Bicicleta.findByCode', () => {
                it('Devuelve bici por id', (done) => {
                    Bicicleta.allBicis((err, bicis) => {
                        expect(bicis.length).toBe(0);

                        var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
                        Bicicleta.add(aBici, (err, newBici) => {
                            if (err) console.log(err);
                            var aBici2 = new Bicicleta({ code: 2, color: "roja", modelo: "urbana" });
                            Bicicleta.add(aBici2, (err, newbici) => {
                                if (err) console.log(err);
                                Bicicleta.findByCode(1, (err, targetBici) => {
                                    expect(targetBici.code).toBe(aBici.code);
                                    expect(targetBici.color).toBe(aBici.color);
                                    expect(targetBici.modelo).toBe(aBici.modelo);
                                    done();
                                })
                            })

                        })

                    })
                })
            })
        });
    });
});

/* beforeEach(()=>{ Bicicleta.allBicis=[];})
describe('Bicicletas.allBicis', ()=>{
    it("Comienza vacio", ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
    })
});

describe("Bicicleta.add", ()=>{
    it("Agragamos una", ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'rojo', 'urbana', [3.4400284,-76.5220833]);


        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    })
})

describe("Bicicleta.findById", ()=>{
    it("Devuelve Bici con id 1", ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'rojo', 'urbana');
        var b = new Bicicleta(2, 'blanco', 'urbana');
        Bicicleta.add(a);
        Bicicleta.add(b);
        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(a.color);
        expect(targetBici.modelo).toBe(a.modelo);
    })
})

describe("Bicicleta.removeById", ()=>{
    it("Elimina Bici con id 1", ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'rojo', 'urbana');
        var b = new Bicicleta(2, 'blanco', 'urbana');
        Bicicleta.add(a);
        Bicicleta.add(b);
        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(1)
        expect(Bicicleta.allBicis[0].id).toBe(2)
    })
}) */