var mongoose = require('mongoose');
var Reserva = require('../../models/reserva');
var Usuario = require('../../models/usuario');
var Bicicleta = require('../../models/bicicleta');


describe("Reserva API", () => {
    beforeEach((done) => {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Connection error'));
        db.once('open', () => {
            console.log('We are connected to test database');
            done();
        });

        afterEach((done) => {
            Reserva.deleteMany({}, (err, success) => {
                if (err) console.log(err);
                Usuario.deleteMany({}, (err, success) => {
                    if (err) console.log(err);
                    Bicicleta.deleteMany({}, (err, success) => {
                        if (err) console.log(err);
                        done();
                    });
                });
            });
        })
    });
})
