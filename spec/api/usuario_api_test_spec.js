var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
var mongoose = require('mongoose');

var baseUrl = 'http://localhost:3000/api/usuarios';

describe('Usuarios API', () => {
    beforeEach((done) => {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Connection error'));
        db.once('open', () => {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach((done) => {
        Reserva.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            Usuario.deleteMany({}, (err, success) => {
                if (err) console.log(err);
                Bicicleta.deleteMany({}, (err, success) => {
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

    /* describe('usuario/reserva', () => {
        it('Crea una reserva de bici', (done) => {
            var headers = { 'Content-Type': 'application/json' };
            var aBici = new Bicicleta({ code: 10, color: "rojo", modelo: "urbana", lat: 34, lon: -76 });
            aBici.save();

            var user = new Usuario({ nombre: "Juan" });
            user.save();
            var reserBici = `{ "_id": "${user._id}", "bici_id": "${aBici._id}", "desde": "2020-08-10", "hasta": "2020-08-11" }`;
            //var aSave = { desde: "2020-08-10", hasta: "2020-08-11", bici_id: aBici._id, id: user._id };
            console.log(reserBici);
            //var id_user= user._id;
            //var id_bici= aBici._id;
            request.post({
                headers: headers,
                url: baseUrl + '/reservar',
                body: reserBici
            }, function (err, response, body) {
                if(err) console.log(err);
                console.log(body);
                expect(response.statusCode).toBe(200);
                Reserva.find({}).populate('bicicleta').populate('usuario').exec((err, reservas) => {
                    if (err) console.log(err);
                    done();
                })
            })

        })
    }) */
});