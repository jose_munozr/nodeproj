var Bicicleta = require("../../models/bicicleta");
var request = require('request');
var server = require('../../bin/www');
var mongoose = require('mongoose');

var baseUrl = 'http://localhost:3000/api/bicicletas';

describe("Bicicleta API", () => {
    beforeEach((done) => {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Connection error'));
        db.once('open', () => {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    describe('GET Bicicletas /', () => {
        it("StatusCode 200", (done) => {
            request.get(baseUrl, (err, response, body) => {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST Bicicletas /create', () => {
        it("StatusCode 200", (done) => {
            var headers = { 'Content-Type': 'application/json' };
            var aBici = '{"code": 11, "color": "rojo", "modelo": "urbana", "lat": 34, "lon": -76}';
            request.post({
                headers: headers,
                url: baseUrl + '/create',
                body: aBici
            }, (err, response, body) => {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body);
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(34);
                expect(bici.ubicacion[1]).toBe(-76);
                done();
            });
        });
    });

    describe('DELETE Bicicletas /delete', () => {
        it("StatusCode 204", (done) => {
            var a = Bicicleta.createInstance(1, "negro", "Montaña", [34, -76]);
            Bicicleta.add(a, (err, newBici) => {
                var headers = { 'Content-Type': 'application/json' };
                var aBici = '{"code": 1}';
                request.delete({
                    headers: headers,
                    url: baseUrl + '/delete',
                    body: aBici
                }, (err, response, body) => {
                    console.log(body);
                    expect(response.statusCode).toBe(204);
                    //expect(Bicicleta.findById(1).color).toBe("rojo");
                    //expect(Bicicleta.allBicis[0].id).toBe(1);
                    done();
                })
            }) 
            /* var headers = { 'Content-Type': 'application/json' };
            var aBici = new Bicicleta({ code: 10, color: "rojo", modelo: "urbana", lat: 34, lon: -76 });
            aBici.save();
            var aDelete = '{"code": 1}';
            request.delete({
                headers: headers,
                url: baseUrl + '/delete',
                body: aDelete
            }, (err, response, body) => {
                expect(response.statusCode).toBe(204);
                done();
            }) */
        })
    })
});
/*
describe("Bicicleta API", ()=>{
    describe("GET /", ()=>{
        it("Status 200", ()=>{
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'rojo', 'urbana', [3.4400284,-76.5220833]);
            //var b = new Bicicleta(2, 'blanca', 'ruta', [3.440436, -76.516096]);

            Bicicleta.add(a);
            request.get('http://localhost:3000/api/bicicletas', (error, response, body)=>{
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe("POST Bicicletas/create", ()=>{
        it("Status 200", (done)=>{
            var headers={'Content-Type': 'application/json'};
            var aBici='{"id": 1, "color": "rojo", "modelo": "urbana", "lat": 34, "lon": -76}';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, (error, response, body)=>{
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("rojo");
                done();
            });
        });
    });

    describe("DELETE Bicicletas/delete", ()=>{
        it("Status 204", (done)=>{
            var headers={'Content-Type': 'application/json'};
            var aBici='{"id": 1}';
            request.delete({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: aBici
            }, (error, response, body)=>{
                expect(response.statusCode).toBe(204);
                expect(Bicicleta.findById(1).color).toBe("rojo");
                //expect(Bicicleta.allBicis[0].id).toBe(1);
                done();
            })
        })
    })
}); */


