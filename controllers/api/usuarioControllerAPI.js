var Usuario = require('../../models/usuario');


exports.usuarios_list = function (req, res) {
    Usuario.find({}, (err, usuarios) => {
        res.status(200).json({
            usuarios: usuarios
        });
    });
}

exports.usuarios_create = function (req, res) {
    console.log(req.body);
    var user = new Usuario({ nombre: req.body.nombre, email: req.body.email, password: req.body.password });

    user.save((err) => {
        if (err) return res.status(500).json(err);
        res.status(200).json(user);
    });
}


exports.usuario_reservar = function (res, req) {
    console.log(req.body);
    Usuario.findById(req.body._id, (err, usuario) => {
        console.log(usuario);
        usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, (err) => {
            if (err) console.log(err);
            console.log('reserva Exitosa!!!!');
            res.status(200).send();
        });
    });
}